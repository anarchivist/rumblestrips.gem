# rumblestrips' journal

## 2025

=> 2025/2025-01-22-blear.gmi 2025-01-22 blear
=> 2025/2025-01-19-time.gmi 2025-01-19 time is nothing but not amenable

## 2024

=> 2024/2024-08-23-sisterhood.gmi 2024-08-23 sisterhood/recordhood
=> 2024/2024-05-04-basking-in-puce.gmi 2024-05-04 basking in puce
=> 2024/2024-04-14-not-knowing.gmi 2024-04-14 the not knowing: cage and calvinism
=> 2024/2024-04-13-stage-whisper.gmi 2024-04-13 in a stage whisper: silence, embodiment, and trans* archival praxis
=> 2024/2024-04-09-citation.gmi 2024-04-09 citation and/in context of gemini
=> 2024/2024-04-04-peak-performance.gmi 2024-04-04 peak performance

---

=> .. homb
=> / estradiol.cloud