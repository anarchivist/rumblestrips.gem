# 2024-04-10 feast of the third day

* candle lit by lighter and match
* rod and amulet removed from bowl
* old salt cleansed from bowl
* new salt placed in bowl
* plant watered
* tarot reading [1]: CESWN - temperance, ace of wands reversed, 8 of moons reversed, the lovers, king of pentacles
* rod and amulet placed in bowl
* invocation and supplication: love, creativity, strength, no impinging on another person's will
* candle extingushed without breath
* isopsephic onomancy based on common names [2], [3], [4]:

```
(Name 1) = 152
Stoicheic = Η (Venus), thus Oracle:
            “Bright Helios [Sun] {Hêlios}, who watches everything, watches you.”
Element = Water
Planet = Mars
Zodiac = Scorpio
Peer isopsephic words and phrases:
    ῥῆγμα (downfall, breakage, rupture)
    ἄλκαρ (safeguard; defense; protection; remedy)

(Name 2) = 151
Stoicheic = Ζ (Cancer), thus Oracle:
            “Flee the very great storm {Zalê}, lest you be disabled in some way.”
Element = Earth 
Planet = Sun
Zodiac = Libra
Peer isopsephic words and phrases:
    κονία (dust; sand; ash; powder to grip during wrestling)
    ὄμμα (a sight; "the eye of heaven; i.e. the sun"; light; "anything dear or precious")
    από (from; than)
    γάλα καὶ μέλι (milk and honey)
    ἡ πάγκαλ (the very beautiful [woman])
    ἡ καρδίη (the heart; the inclination; the desire [Ionic]) 

(Name 2, Greek form) = 145
Stoicheic = Α (Moon), thus Oracle: 
            “The God [Apollo] says you will do everything {Hapanta} successfully.”
Element = Aether
Planet = Mars
Zodiac = Aries
Peer isopsephic words and phrases:
    αἱ κηπεῖαι (the gardens)
    βαλλίζειν (to dance)
    γεγέννηκα (I have begotten; produced; engendered; call[ed] into existence)
    ἡ Ἀρκαδία (Arcadia)
    δέλεαρ (bait; incitement)
    δόξαι (expectations; fancy; vision; good repute)
    ἀνενέγκαι (to offer up; to bring; uphold; yield)
```

---

=> 2024-04-10.jpg [1] 2024-04-10 tarot reading
=> https://digitalambler.com/rituals/stoicheia/ [2] gematria, isopsephy, and stoicheia
=> https://digitalambler.com/2014/11/08/greek-onomancy-linking-isopsephy-with-stoicheia/ [3] greek onomancy: linking isopsephy with stoicheia
=> http://opsopaus.com/OM/BA/GAO.html [4] a greek alphabet oracle

---

=> .. homb
=> / estradiol.cloud
