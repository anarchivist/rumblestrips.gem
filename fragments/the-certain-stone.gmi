# the certain stone

i long to be wholly certain, just to be
certain of how we all will thrive, just to be
certain of a beautiful tomorrow with electric skies. 
certain of near-limitless energy within us:
that we nudge cherry trees into bloom and fruit,
that we heave the sun and moon into their places,
that we chart the exact trajectories of our parts
meting out and merging into a certain whole.

how does imperfect information build certainty?
through tacit models or conceptual knowledges?
as stepladders for real beliefs or true feelings?
using fictionalized observations or keen histories?
on a dais of well-considered entropy or absolute order?
i'd sooner split atoms with my lashes knit shut
than to be wholly certain that a fading map's
creases are riverbeds, cherry orchards or gravel roads.

we are told, "here is one hand and here is another":
certain truth that the external world must exist.
hands can hold fists and other hands back, inside
and together. but what exists outside the hands?
inside them lies a potential to fracture reality.
tunneling through us as breaking waves on strings, 
fusing our bodies while splitting our cores:
a holy certain stone rending the cherry in two.

--- 

```
note: work in progress
lastmod: 2024-08-18
```

---

=> .. homb
=> / estradiol.cloud