# caelum iii/juglans cinerea 

i felt the sun pour down into molten bronze on my shoulders: casting me into an unlikely statue. you, the sculptor, could intuit how my form would take to metal. the crucible emptied itself. as you cooled these familiar, imperfect shapes, you saw a channel for every sinew, wrinkle, and pockmarked bit of flesh. these channels became another set of constellations bearing fruit as a future zodiac. you said i was born under the sign of the feather (hammer rising): lightness and gravity, twin yet unlikely forces that worked in concert. motionless, i agreed with you as bead upon bead pooled into my navel, ears, and nailbeds. 

the pull of each of them ever expanding, distances became immeasurable on a galactic scale. the speed of light became an irrational footnote. single, simple glyphs were scattered throughout the electromagnetic spectrum, arriving in whatever sequence they demanded, rather than perfectly ordered messages. these were our own peculiar velocities: the smoothness of our own bodies in sympathetic orbits. i watched red turn to blue and hover in violet. glyphs became syllables, turning into words, phrases, and verse. you read them aloud to me in the same voice i remember: tender as a nutcracker's teeth finding the point on a walnut's shell to free the flesh inside. 

i solidified again in the cold of space. flares licked my cheeks and nose as i got closer to the sun. another sculptor, another forge, finding new channels. the new constellations remained. venus was in the elbow, you said, calling out countless new names for all the unpredictable heavenly eccentricities that stood before us. i put my arms around your stomach and told you when i thought the sun would rise next. you grinned and asked me when it would set. we both knew it would only reach past the horizon when we stopped wanting it in the sky. 

=> caelum-ii.gmi backward (in the sky)
=> juglans-regia.gmi previously (in the trees)

--- 

```
note: work in progress 
lastmod: 2024-08-31
```

---

=> .. homb
=> / estradiol.cloud