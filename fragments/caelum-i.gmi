# caelum i

a blank-faced chair catches the glow of soft light from above. its bare seat, awaiting its chance at comforting lumbosacral curves, also shimmers, upholstered in azure pleather. asterisms reveal themselves through its tight stitching, their names pealing through the vacuum of space, a carillon ringing in an empty sky. the chair looks forward onto warm, angular wood (firm with false pliancy from radiant heat), midnight walls supple with preserved moss invite anonymous, unfamiliar, tender hands. it respires, sighs, looks out across at the upright chair, awaits return of its occupant, longs for the refrain of the chorus from the speakers.

the song starts the same the last time it was on the radio, speaker facing the corner of the bar you stand at patiently, brow furrowed, waiting for glassware containing healthy pours. the cold of space reverberating the shine of the winter hexagon: Pollux awaits Castor. Aldebaran awaits Porrima. i rap two knuckles on the table twice thinking of each pair, skin stinging with splinters. Helen awaits Clytemnestra. one more rap, one more splinter. you turn away from the corner, glidepath icy, confident, holding stemware in each hand like single calla lillies to grace a marble ledge. impossibly alabaster against the midnight wall.

upon your arrival: the chair sighs, not the wall, its ultimate purpose complete as new clusters of stars reveal themselves. we nestle deeply as each of us escalate mutual confessions and absolution. more peals, this time resounding chimes of laughter, reverberating through The Tower of the Winds, illuminating its meridian lines. we observe and speculate about where we might predict the new ways the coma glows as it approaches the sun. sublimating the hairs over our hearts, our wild tails spin with increasing volatility. we continue rotating in until we leave our seats in osculating orbit. 

=> caelum-ii.gmi forward

--- 

```
note: work in progress but unearthed
lastmod: 2024-08-24
```

---